package com.aswdc.matrimony.model;

import java.io.Serializable;

public class CityModel implements Serializable {

    int CityID;
    String Name;

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
