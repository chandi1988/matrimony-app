package com.aswdc.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.UserListAdapter;
import com.aswdc.matrimony.database.TblUser;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends BaseActivity {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_userlist), true);
        setAdapter();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {

            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserID = new TblUser(UserListActivity.this).updateFavoriteStatus(0, userList.get(position).getUserId());
                if (lastUpdatedUserID > 0) {
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0, userList.size());
                }
            }

            @Override
            public void OnItemClick(int position) {
                Intent intent = new Intent(getApplicationContext(), AddUserActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);
            }
        });
        rcvUserList.setAdapter(adapter);
    }
}
