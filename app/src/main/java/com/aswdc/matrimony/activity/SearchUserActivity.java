package com.aswdc.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.UserListAdapter;
import com.aswdc.matrimony.database.TblUser;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchUserActivity extends BaseActivity {

    @BindView(R.id.etUserSearch)
    EditText etUserSearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;

    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_search_user), true);
        setAdapter();
        setSearchUser();
    }

    void resetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    void setSearchUser() {
        etUserSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tempUserList.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < userList.size(); j++) {
                        if (userList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getSurName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            tempUserList.add(userList.get(j));
                        }
                    }
                }
                if (charSequence.toString().length() == 0 && tempUserList.size() == 0) {
                    tempUserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    void setAdapter() {
        rcvUsers.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getUserList());
        tempUserList.addAll(userList);
        adapter = new UserListAdapter(this, tempUserList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {

            }

            @Override
            public void onFavoriteClick(int position) {

            }

            @Override
            public void OnItemClick(int position) {
                Intent intent = new Intent(getApplicationContext(), AddUserActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);
            }
        });
        rcvUsers.setAdapter(adapter);
    }
}
